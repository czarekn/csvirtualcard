﻿using CSVirtualCard;
using System;

namespace CSVirtualCardApp
{
    class Program
    {
        static void Main(string[] args)
        {
            var account = new Account();
            var card = new VirtualCard("1111-2222-3333-4444", "1234", account);

            card.Deposit(100);
            card.Deposit(99);
            try
            {
                card.Debit(29, "12345");
            }
            catch { }
            card.Debit(129, "1234");

            Console.WriteLine(account.Balance);
            Console.ReadLine();
        }
    }
}
