﻿using System;

namespace CSVirtualCard.Interfaces
{
    public interface IAccount
    {
        Decimal Balance { get; }
        Decimal Deposit(Decimal amount);
        Decimal Debit(Decimal amount);
    }
}
