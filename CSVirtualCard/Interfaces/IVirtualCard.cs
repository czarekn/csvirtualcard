﻿using System;

namespace CSVirtualCard.Interfaces
{
    public interface IVirtualCard
    {
        string CardNumber { get; }
        bool Debit(Decimal amount, string pin);
        bool Deposit(Decimal amount);
    }
}
