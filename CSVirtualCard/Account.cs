﻿using CSVirtualCard.Interfaces;
using System;

namespace CSVirtualCard
{
    public class Account : IAccount
    {
        private readonly object m_syncObject = new Object();
        private Decimal m_balance;

        public decimal Balance => m_balance;

        public Decimal Debit(decimal amount)
        {
            if (amount < 0)
                throw new InvalidOperationException("Amount must be non-negative");

            lock (m_syncObject)
            {
                if (amount > Balance)
                    throw new InvalidOperationException("Amount too large");

                m_balance -= amount;

                return Balance;
            }
        }

        public Decimal Deposit(decimal amount)
        {
            if (amount < 0)
                throw new InvalidOperationException("Amount must be non-negative");

            lock (m_syncObject)
            {
                m_balance += amount;
                return Balance;
            }
        }
    }
}
