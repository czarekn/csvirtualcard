﻿using CSVirtualCard.Interfaces;
using System;

namespace CSVirtualCard
{
    public class VirtualCard : IVirtualCard
    {
        private readonly IAccount m_account;
        private readonly string m_cardNumber;
        private string m_pin;

        public string CardNumber => m_cardNumber;

        public VirtualCard(string cardNumber, string pin, IAccount account)
        {
            m_account = account;
            m_cardNumber = cardNumber;
            m_pin = pin;
        }

        public bool Debit(decimal amount, string pin)
        {
            Authorize(pin);
            m_account.Debit(amount);
            return true;
        }

        public bool Deposit(decimal amount)
        {
            m_account.Deposit(amount);
            return true;
        }

        private void Authorize(string pin)
        {
            if (!m_pin.Equals(pin))
                throw new InvalidOperationException("Invalid PIN");
        }
    }
}
