﻿using CSVirtualCard;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace CSVirtualCardTests
{
    [TestClass]
    public class AccountTests
    {
        [TestMethod]
        public void CreatesAccountWithZeroBalance()
        {
            // Arrange
            // Act
            var account = new Account();

            // Assert
            Assert.IsNotNull(account);
            Assert.AreEqual(0m, account.Balance);
        }

        [TestMethod]
        public void Deposit_IncreasesAccountBalance()
        {
            // Arrange
            var account = new Account();

            // Act
            var result = account.Deposit(121m);

            // Assert
            Assert.AreEqual(121m, result);
            Assert.AreEqual(121m, account.Balance);
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]
        public void Deposit_WhenAmountNegative_DoesNotChangeBalanceAndThrowsException()
        {
            // Arrange
            var account = new Account();

            try
            {
                // Act
                var result = account.Deposit(-121m);
            }
            catch(Exception e)
            {
                Assert.AreEqual(0m, account.Balance);
                throw;
            }
        }

        [TestMethod]
        public void Debit_DecreasesAccountBalance()
        {
            // Arrange
            var account = new Account();
            account.Deposit(121m);

            // Act
            var result = account.Debit(22m);

            // Assert
            Assert.AreEqual(99m, result);
            Assert.AreEqual(99m, account.Balance);
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]
        public void Debit_WhenAmountNegative_DoesNotChangeBalanceAndThrowsException()
        {
            // Arrange
            var account = new Account();
            account.Deposit(121m);

            try
            {
                // Act
                var result = account.Debit(-121m);
            }
            catch (Exception e)
            {
                Assert.AreEqual(121m, account.Balance);
                throw;
            }
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]
        public void Debit_WhenAmountTooLarge_DoesNotChangeBalanceAndThrowsException()
        {
            // Arrange
            var account = new Account();
            account.Deposit(121m);

            try
            {
                // Act
                var result = account.Debit(122m);
            }
            catch (Exception e)
            {
                Assert.AreEqual(121m, account.Balance);
                throw;
            }
        }
    }
}
