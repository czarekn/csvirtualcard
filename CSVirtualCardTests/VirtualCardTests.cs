﻿using CSVirtualCard;
using CSVirtualCard.Interfaces;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NMock;
using System;

namespace CSVirtualCardTests
{
    [TestClass]
    public class VirtualCardTests
    {
        private MockFactory m_factory;
        private Mock<IAccount> m_account;

        [TestInitialize]
        public void Initialize()
        {
            m_factory = new MockFactory();
            m_account = m_factory.CreateMock<IAccount>();
        }

        [TestCleanup]
        public void Cleanup()
        {
            m_factory.VerifyAllExpectationsHaveBeenMet();
            m_factory.ClearExpectations();
        }

        [TestMethod]
        public void CreatesVirtualCardWithZeroBalance()
        {
            // Arrange
            // Act
            var virtualCard = new VirtualCard("card-no", "pin-no", m_account.MockObject);

            // Assert
            Assert.IsNotNull(virtualCard);
            Assert.AreEqual("card-no", virtualCard.CardNumber);
        }

        [TestMethod]
        public void Deposit_IncreasesVirtualCardBalance()
        {
            // Arrange
            m_account.Expects.One.Method(m => m.Deposit(121m)).With(Is.EqualTo(121m)).WillReturn(121m);
            var virtualCard = new VirtualCard("card-no", "pin-no", m_account.MockObject);

            // Act
            var result = virtualCard.Deposit(121m);

            // Assert
            Assert.AreEqual(true, result);
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]
        public void Deposit_WhenAmountNegative_DoesNotChangeBalanceAndThrowsException()
        {
            // Arrange
            m_account.Expects.One.Method(m => m.Deposit(-121m)).With(Is.EqualTo(-121m)).Will(Throw.Exception(new InvalidOperationException()));
            var virtualCard = new VirtualCard("card-no", "pin-no", m_account.MockObject);

            // Act
            virtualCard.Deposit(-121m);
        }

        [TestMethod]
        public void Debit_DecreasesVirtualCardBalance()
        {
            // Arrange
            m_account.Expects.One.Method(m => m.Debit(22m)).With(Is.EqualTo(22m)).WillReturn(99m);
            var virtualCard = new VirtualCard("card-no", "pin-no", m_account.MockObject);

            // Act
            var result = virtualCard.Debit(22m, "pin-no");

            // Assert
            Assert.AreEqual(true, result);
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]
        public void Debit_WhenAmountNegative_DoesNotChangeBalanceAndThrowsException()
        {
            // Arrange
            m_account.Expects.One.Method(m => m.Debit(-121m)).With(Is.EqualTo(-121m)).Will(Throw.Exception(new InvalidOperationException()));
            var virtualCard = new VirtualCard("card-no", "pin-no", m_account.MockObject);

            // Act
            var result = virtualCard.Debit(-121m, "pin-no");
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]
        public void Debit_WhenAmountTooLarge_DoesNotChangeBalanceAndThrowsException()
        {
            // Arrange
            m_account.Expects.One.Method(m => m.Debit(122m)).With(Is.EqualTo(122m)).Will(Throw.Exception(new InvalidOperationException()));
            var virtualCard = new VirtualCard("card-no", "pin-no", m_account.MockObject);

            // Act
            virtualCard.Debit(122m, "pin-no");
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]
        public void Debit_WhenPinWrong_DoesNotChangeBalanceAndThrowsException()
        {
            // Arrange
            var virtualCard = new VirtualCard("card-no", "pin-no", m_account.MockObject);

            // Act
            virtualCard.Debit(12m, "pin-no-wrong");
        }
    }
}
